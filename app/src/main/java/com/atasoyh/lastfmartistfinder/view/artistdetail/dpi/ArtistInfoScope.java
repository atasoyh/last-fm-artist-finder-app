package com.atasoyh.lastfmartistfinder.view.artistdetail.dpi;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by atasoyh on 10/07/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ArtistInfoScope {
}
